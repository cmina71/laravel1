  @extends('app')
  
  
  @section('content')

    <div class="container w-25 border p-4 mt-4">
      <div class="pb-4">
        <form action="{{ route('guardar')}}" method="post">
          @csrf
          @method('post')
          @if(session('Success'))
            <h6 class="alert alert-success">{{ session('Success') }}</h6>

          @endif
      </div>

          @error('title')
          <h6 class="alert alert-danger ">{{ $message }}</h6>
          @enderror
            <div class="mb-3">
            <label for="title" class="form-label">Título de la tarea</label>
            <input type="text" class="form-control" name="title" required>
            <div id="emailHelp" class="form-text">Digita de el nombre que le asignaras a tu tarea.</div>
            </div>
            <div class="d-grid gap-2 d-md-flex justify-content-md-end">
            <input class="btn btn-primary me-md-2" type="submit" value="Guardar"></input>
            </div>
        </form>

        <div>
          <hr/>
          {{ $slot }}
        </div>
    </div>



  @endsection