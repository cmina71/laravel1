<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tarea extends Model
{
    use HasFactory;

    public $timestamps = false;

    /**
     *  La tabla asociada con el modelo
     */

    protected $tabla = 'task_manager';

    /**
     * 
     * La llave primaria asociada con la tabla
     */

    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'table'
    ];
}
