<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tarea;

class TareasController extends Controller
{
    /**
     * index para mostrar todos las tareas
     * store para guardar una tarea
     * update para actualizar las tareas
     * destoy para eliminar una tarea
     * edit para mostrar el formulario de edición 
     */

     public function store(Request $request){
        $request->validate([
            'title' => 'required|min:3'
        ]);

        $tarea = new Tarea;
        $tarea->title = $request->title;
        $tarea->save();

        return redirect()->route('guardar')->with('Success', 'Tarea creada exitosamente');

     }

     public function index() {
        $tarea = Tarea::all();
        //dd($tarea);
        return view('tareas.showtasks', ['tareas' => $tarea]);
     }

     public function show($id) {
        $tarea = Tarea::find($id);
        //dd($tarea);
        return view('tareas.show', ['tarea' => $tarea]);
     }

     public function update() {
        $tarea = Tarea::all();
        //dd($tarea);
        return view('tareas.index', ['tarea' => $tarea]);
     }


     public function destroy() {
        $tarea = Tarea::all();
        //dd($tarea);
        return view('tareas.index', ['tarea' => $tarea]);
     }
      
}

