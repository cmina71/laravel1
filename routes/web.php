<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TareasController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/tareas');
});


Route::get('/tareas', [TareasController::class, 'index'])->name('tareas');

Route::post('/guardar', [TareasController::class, 'store'])->name('guardar');

Route::get('/tarea/{id}', [TareasController::class, 'show'])->name('tarea-show');



Route::patch('/tareas', [TareasController::class, 'store'])->name('tarea-update');
Route::delete('/tareas', [TareasController::class, 'store'])->name('tarea-destroy');



