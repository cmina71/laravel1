
<x-layout>
  <x-slot:title>
      Custom Title 
  </x-slot>
@foreach ($tareas as $task)
            <div class="row py-1 mt-3 border-top border-secondary">
              <div class="col-md-9 d-flex align-items-center">
                <a href="{{ route('tarea-show', ['id' => $task->id])}}">{{ $task->title }}</a>
              </div>

              <div class="col-md-3 d-flex justify-content-end">
                <a href="{{ route('tarea-destroy', [$task->id]) }}"></a>
                  <input type="submit" class="btn btn-danger btn-sm" value="Eliminar">
              </div>
            </div>
            
@endforeach
</x-layout>


<?php
Route::get('/tareas', function(){
  return view('tareas', ['task' => Task::all()]);
})
?>