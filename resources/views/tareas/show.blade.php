@extends('app')

  @section('content')

    <div class="container w-25 border p-4 mt-4">
        <form action="" method="">
          @csrf
          @method('POST')
          @if(session('Success'))
            <h6 class="alert alert-success">{{ session('Success') }}</h6>
          @endif
        </form>

          @error('title')
          <h6 class="alert alert-danger">{{ $message }}</h6>
          @enderror
            <div class="mb-3">
              <form action="">
                <label for="title" class="form-label">Título de la tarea</label>
                <input type="text" class="form-control" name="title" value="{{ $tarea->title }}" required>
              </form>
            <div class="d-grid gap-2 d-md-flex justify-content-md-end">
              <input class="btn btn-primary me-md-2" type="submit" value="Actualizar tarea">Actualizar tarea</input>
            </div>
    </div>